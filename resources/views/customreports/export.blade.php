<table class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: table;border-collapse: collapse">
    <thead>
    <tr>
        <th>Name</th>
        @foreach($fields as $key => $val)
            @if($val != null)
                <th>{{$val}}</th>
            @endif
        @endforeach
    </tr>
    </thead>
    <tbody>
    @forelse($clients as $client)
        @if(isset($client['id']))
            <tr>
                <td class="table100-firstcol">{{$client['company']}}</td>
                @foreach($client["data"] as $key => $val)
                    <td>@if($val != strip_tags($val)) {!! $val !!} @else {{$val}} @endif</td>
                @endforeach
            </tr>
        @endif
    @empty
        <tr>
            <td colspan="100%" class="text-center"><small class="text-muted">No patients match those criteria.</small></td></td>
        </tr>
    @endforelse
    </tbody>
</table>